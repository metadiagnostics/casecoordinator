package io.saperi.virtualhealth.casecoordinator.handlers;

import io.saperi.common.communications.interfaces.IPubSubEventCallback;
import io.saperi.common.communications.listeners.TypeMappedListener;
import io.saperi.virtualhealth.caseagent.shared.event.CaseAgentAlive;
import io.saperi.virtualhealth.casecoordinator.components.SharedContext;
import io.saperi.virtualhealth.casecoordinator.data.AliveRecord;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Date;
import java.util.Map;

@Slf4j
@Component
public class KeepAliveChannelHandler extends TypeMappedListener {

    private SharedContext context;

    public KeepAliveChannelHandler(SharedContext context) {
        this.context = context;
    }

    @PostConstruct
    public void setupHandlers() {
        this.addMapping(CaseAgentAlive.class, new KeepAliveChannelHandler.handleCaseAgentAlive());
    }

    class handleCaseAgentAlive implements IPubSubEventCallback {

        @Override
        public void handleEvent(String topic, String type, Object obj) {
            CaseAgentAlive evt = (CaseAgentAlive) obj;
            Date now = new Date();
            AliveRecord data;
            Map<String, AliveRecord> lastSeenMap = context.getLastSeensMap();
            String agentId = evt.getCaseAgentId();
            //We have two paths here depending on if the agent has already reported existence
            if (lastSeenMap.containsKey(agentId)) {
                data = lastSeenMap.get(agentId);
                updateAliveRecord(data, evt, now);
            } else {
                //The Agent is new, We create the record intact before placing it in the map to make sure the monitoring job has a complete record
                data = new AliveRecord();
                updateAliveRecord(data, evt, now);
                lastSeenMap.put(agentId, data);
            }
        }

        /**
         * Common Record update logic
         *
         * @param data
         * @param evt
         * @param now
         */
        private void updateAliveRecord(AliveRecord data, CaseAgentAlive evt, Date now) {
            data.setReceived(now);
            data.setRemote(evt.getTime());

        }
    }
}

