package io.saperi.virtualhealth.casecoordinator.controller;

import io.saperi.virtualhealth.caseagent.shared.event.CensusRequest;
import io.saperi.virtualhealth.caseagent.shared.event.Status;
import io.saperi.virtualhealth.casecoordinator.components.EventFactory;
import io.saperi.virtualhealth.casecoordinator.components.SharedContext;
import io.saperi.virtualhealth.casecoordinator.data.CaseData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@Slf4j
@CrossOrigin(origins = "*")
public class ManagementController {

    private final EventFactory eventFactory;
    private final SharedContext context;

    public ManagementController(EventFactory eventFactory, SharedContext context) {
        this.eventFactory = eventFactory;
        this.context = context;
        log.info("Started");
    }

    @GetMapping("/alive")
    public Status alive()
    {
        Status out = new Status();
        out.setTimeStarted(context.getStarted());
        return out;
    }


    @GetMapping("/agents")
    public   Map<String, CaseData>  agents()
    {
        Map<String, CaseData> out = context.getCaseMap();
        return out;
    }

    @GetMapping("/requestCensus")
    public   String requestCensus()
    {
        CensusRequest evt = eventFactory.createCensusRequestEvent(context);

        return "NYI";
    }

}
