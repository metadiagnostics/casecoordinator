package io.saperi.virtualhealth.casecoordinator.handlers;

import io.saperi.common.communications.interfaces.IPubSubEventCallback;
import io.saperi.common.communications.interfaces.IPubSubProvider;
import io.saperi.common.communications.listeners.TypeMappedListener;
import io.saperi.virtualhealth.caseagent.shared.event.CaseAgentStarted;
import io.saperi.virtualhealth.caseagent.shared.event.CaseAgentStopped;
import io.saperi.virtualhealth.caseagent.shared.event.CensusResponse;
import io.saperi.virtualhealth.casecoordinator.components.SharedContext;
import io.saperi.virtualhealth.casecoordinator.data.CaseData;
import io.saperi.virtualhealth.casecoordinator.shared.event.CoordinatorStarted;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.*;

@Slf4j
@Component
public class ManagementChannelHandler extends TypeMappedListener {

    private final IPubSubProvider pubSubSrv;
    private final CaseHandler caseHandler;
    private SharedContext ctx;

    public ManagementChannelHandler(SharedContext ctx, IPubSubProvider pubSubSrv, CaseHandler caseHandler) {
        this.ctx = ctx;
        this.pubSubSrv = pubSubSrv;
        this.caseHandler = caseHandler;
    }

    @PostConstruct
    public void setupHandlers() {
        this.addMapping(CoordinatorStarted.class, new handleCoordinatorStarted());
        this.addMapping(CaseAgentStarted.class, new handleCaseAgentStarted());
        this.addMapping(CaseAgentStopped.class, new handleCaseAgentStopped());
    }

    class handleCoordinatorStarted implements IPubSubEventCallback {

        @Override
        public void handleEvent(String topic, String type, Object obj) {
            CoordinatorStarted evt = (CoordinatorStarted) obj;
            if (evt.getAgentId().compareTo(ctx.getAgentId()) == 0) {
                log.info("Got our Coordinator Started Event");
                //
            } else {
                log.info("Got Coordinator Started Event for Agent {}", evt.getAgentId());
            }

        }
    }

    class handleCaseAgentStarted implements IPubSubEventCallback {

        @Override
        public void handleEvent(String topic, String type, Object obj) {
            CaseAgentStarted evt = (CaseAgentStarted) obj;
            log.info("Got a CaseAgent Started Event for agent {}", evt.getCaseAgentId());


            //Ok do we know this agent?
            String caseAgentId = evt.getCaseAgentId();
            Map<String, CaseData> caseMap = ctx.getCaseMap();
            if (caseMap.containsKey(caseAgentId)) {
                // Yes - Then the agent is likely restarted - Update data as required and make sure the agent is in service
                CaseData currentInfo = caseMap.get(caseAgentId);
                if (currentInfo.getCaseGeneralInfoTopic().compareTo(evt.getOutputTopic()) != 0) {
                    //Unsubscribe to the old channel
                    pubSubSrv.unsubscribeToTopic(currentInfo.getCaseGeneralInfoTopic(), caseHandler);
                    //Subscribe to the new channel
                    pubSubSrv.createTopic(evt.getOutputTopic());
                    pubSubSrv.subscribeToTopic(evt.getOutputTopic(), caseHandler);
                    currentInfo.setCaseGeneralInfoTopic(evt.getOutputTopic());
                }
                currentInfo.setCaseRequestTopic(evt.getInputTopic());
            } else {
                // No - Setup agent for access and monitoring
                CaseData data = new CaseData();
                data.setAgentId(caseAgentId);
                data.setCaseRequestTopic(evt.getInputTopic());
                data.setCaseGeneralInfoTopic(evt.getOutputTopic());
                //We don't have demographics since the case is just started and not open.
                caseMap.put(caseAgentId, data);
                pubSubSrv.createTopic(evt.getOutputTopic());
                pubSubSrv.subscribeToTopic(evt.getOutputTopic(), caseHandler);
                log.info("Subscribed to Case Output Topic {}", evt.getOutputTopic());
            }
        }
    }

    class handleCaseAgentStopped implements IPubSubEventCallback {

        @Override
        public void handleEvent(String topic, String type, Object obj) {
            CaseAgentStopped evt = (CaseAgentStopped) obj;
            String caseAgentId = evt.getCaseAgentId();
            log.info("Got a case agent stopped event for agent {}", caseAgentId);
            Map<String, CaseData> caseMap = ctx.getCaseMap();
            if (caseMap.containsKey(caseAgentId)) {
                //Ok we know this case lets drop
                CaseData currentInfo = caseMap.get(caseAgentId);
                pubSubSrv.unsubscribeToTopic(currentInfo.getCaseGeneralInfoTopic(), caseHandler);
                caseMap.remove(caseAgentId);

                //TODO:  Cleanup resources associated with this case
                // For example the keep alive monitor channel
                ctx.getLastSeensMap().remove(caseAgentId);
            } else {
                //We don't know the agent - It is possible that we just started and a census is pending
                ctx.getRemovedAgentsMap().put(caseAgentId, new Date());
                log.debug("Unknown Case agent stopped on channel, Maybe a Race between Shutdown and census?");
            }
        }
    }

    class handleCensusResponse implements IPubSubEventCallback {

        @Override
        public void handleEvent(String topic, String type, Object obj) {
            CensusResponse evt = (CensusResponse) obj;
            String caseAgentId = evt.getCaseAgentId();
            log.info("Got a CensusResponse Event for agent {}", caseAgentId);

            if (!ctx.getRemovedAgentsMap().keySet().contains(caseAgentId)) {
                //Ok do we know this agent?

                Map<String, CaseData> caseMap = ctx.getCaseMap();
                if (caseMap.containsKey(caseAgentId)) {
                    // Yes - Then the agent is likely restarted - Update data as required and make sure the agent is in service
                    CaseData currentInfo = caseMap.get(caseAgentId);
                    if (currentInfo.getCaseGeneralInfoTopic().compareTo(evt.getOutputTopic()) != 0) {
                        //Unsubscribe to the old channel
                        pubSubSrv.unsubscribeToTopic(currentInfo.getCaseGeneralInfoTopic(), caseHandler);
                        //Subscribe to the new channel
                        pubSubSrv.createTopic(evt.getOutputTopic());
                        pubSubSrv.subscribeToTopic(evt.getOutputTopic(), caseHandler);
                        currentInfo.setCaseGeneralInfoTopic(evt.getOutputTopic());
                    }
                    currentInfo.setCaseRequestTopic(evt.getInputTopic());
                }
                else {
                    // No - Setup agent for access and monitoring
                    CaseData data = new CaseData();
                    data.setAgentId(caseAgentId);
                    data.setCaseRequestTopic(evt.getInputTopic());
                    data.setCaseGeneralInfoTopic(evt.getOutputTopic());
                    //We don't have demographics since the case is just started and not open.
                    caseMap.put(caseAgentId, data);
                    pubSubSrv.createTopic(evt.getOutputTopic());
                    pubSubSrv.subscribeToTopic(evt.getOutputTopic(), caseHandler);
                    log.info("Subscribed to Case Output Topic {}", evt.getOutputTopic());
                }
            }
            else
            {
                //Remove if required
                cleanupOldRemovedCases();
            }
        }

    }

    private void cleanupOldRemovedCases()
    {
        //Scan the map and remove any elements that are to old.
        Date now = new Date();
        Map<String, Date> removedCases = ctx.getRemovedAgentsMap();
        Set<String> keySet = removedCases.keySet();
        for (String key: keySet)
        {
            Date removed = removedCases.get(key);
            //Ok we give entries a generous 5 minutes on the removed list
            if (now.getTime()-removed.getTime()>(5*60*1000))
            {
                removedCases.remove(key);
            }
        }

    }
}
