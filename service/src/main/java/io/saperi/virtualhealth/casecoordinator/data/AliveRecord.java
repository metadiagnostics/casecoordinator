package io.saperi.virtualhealth.casecoordinator.data;

import lombok.Data;

import java.util.Date;

@Data
public class AliveRecord {
    private Date received;
    private Date remote;
}
