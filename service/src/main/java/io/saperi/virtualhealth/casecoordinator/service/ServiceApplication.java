package io.saperi.virtualhealth.casecoordinator.service;


import com.solacesystems.jcsmp.SpringJCSMPFactory;
import io.saperi.common.communications.interfaces.IPubSubProvider;
import io.saperi.virtualhealth.casecoordinator.components.EventFactory;
import io.saperi.virtualhealth.casecoordinator.components.SharedContext;
import io.saperi.virtualhealth.casecoordinator.handlers.KeepAliveChannelHandler;
import io.saperi.virtualhealth.casecoordinator.handlers.ManagementChannelHandler;
import io.saperi.virtualhealth.casecoordinator.handlers.PrivateInputHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.ContextStoppedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.annotation.PreDestroy;

@Slf4j
@EnableWebMvc
@Configuration
@ImportResource("classpath:beans.xml")
@SpringBootApplication
@ComponentScan("io.saperi.virtualhealth.casecoordinator")
//We scan these components mainly to make sure there is at least one pubsub implementation available
@ComponentScan("io.saperi.common.communications")
public class ServiceApplication {

    //Found via the beans.xml and properties file
    private final IPubSubProvider pubSubSrv;

    private final SharedContext context;

    private final ManagementChannelHandler mgmtHandler;
    private final KeepAliveChannelHandler keepAliveChannelHandler;
    private final PrivateInputHandler inputHandler;
    private final EventFactory eventFactory;


    public ServiceApplication(SpringJCSMPFactory solaceFactory, IPubSubProvider pubSubSrv, SharedContext context, ManagementChannelHandler mgmtHandler, KeepAliveChannelHandler keepAliveChannelHandler, PrivateInputHandler inputHandler, EventFactory evtFactory) {
        this.pubSubSrv = pubSubSrv;
        this.context = context;
        this.mgmtHandler = mgmtHandler;
        this.keepAliveChannelHandler = keepAliveChannelHandler;
        this.inputHandler = inputHandler;
        this.eventFactory = evtFactory;
    }


    public static void main(String[] args) {
        SpringApplication.run(ServiceApplication.class, args);
    }

    @EventListener
    public void handleContextRefreshEvent(ContextRefreshedEvent evt)
    {
        log.info("Context Refreshed, "+evt.getApplicationContext().getApplicationName());
        initialize();
        log.info("Initialization  complete");

    }

    @EventListener
    public void handleContextStopped(ContextStoppedEvent evt)
    {
        log.info("Context Stopped, "+evt.getApplicationContext().getApplicationName());


    }

    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**");
            }
        };
    }
        private void initialize()
    {
        pubSubSrv.initialize();

        // Create Topics for Input/Output/Alive
        pubSubSrv.createTopic(context.getManagementTopic());
        //Temp Subscriptions
        pubSubSrv.subscribeToTopic(context.getManagementTopic(),mgmtHandler);
        //
        pubSubSrv.createTopic(context.getRootInputTopic());
        pubSubSrv.createTopic(context.getRootOutputTopic());
        pubSubSrv.createTopic(context.getKeepAliveTopic());
        pubSubSrv.subscribeToTopic(context.getRootInputTopic(),inputHandler);
        pubSubSrv.subscribeToTopic(context.getKeepAliveTopic(),keepAliveChannelHandler);


        //Publish Existence
        pubSubSrv.publishToTopic(context.getManagementTopic(), eventFactory.createCoordinatorStartedEvent(context));

        //Request Census
        pubSubSrv.publishToTopic(context.getManagementTopic(), eventFactory.createCensusRequestEvent(context));


    }



    @PreDestroy
    public void tearDown()
    {
        //Publish Existence
        pubSubSrv.publishToTopic(context.getManagementTopic(), eventFactory.createCoordinatorStoppedEvent(context));
        log.info("Shutting down....");
        pubSubSrv.cleanup();
    }
}
