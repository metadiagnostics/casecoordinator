package io.saperi.virtualhealth.casecoordinator.handlers;

import io.saperi.common.communications.interfaces.IPubSubEventCallback;
import io.saperi.common.communications.listeners.TypeMappedListener;
import io.saperi.virtualhealth.caseagent.shared.event.*;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class CaseHandler extends TypeMappedListener {
    @PostConstruct
    public void setupHandlers()
    {
        this.addMapping(CaseInfo.class, new CaseHandler.handleCaseInfo());
        this.addMapping(DeviceAdded.class, new CaseHandler.handleDeviceAdded());
        this.addMapping(DeviceRemoved.class, new CaseHandler.handleDeviceRemoved());
        this.addMapping(DeviceStatus.class, new CaseHandler.handleDeviceStatus());
        this.addMapping(DeviceChangeException.class, new CaseHandler.handleDeviceChangeException());
        this.addMapping(DeviceChangeAck.class, new CaseHandler.handleDeviceChangeAck());
    }

    class handleCaseInfo implements IPubSubEventCallback {

        @Override
        public void handleEvent(String topic, String type, Object obj) {

        }
    }
    class handleDeviceAdded implements IPubSubEventCallback {

        @Override
        public void handleEvent(String topic, String type, Object obj) {

        }
    }
    class handleDeviceRemoved implements IPubSubEventCallback {

        @Override
        public void handleEvent(String topic, String type, Object obj) {

        }
    }
    class handleDeviceStatus implements IPubSubEventCallback {

        @Override
        public void handleEvent(String topic, String type, Object obj) {

        }
    }

    class handleDeviceChangeAck implements IPubSubEventCallback {

        @Override
        public void handleEvent(String topic, String type, Object obj) {

        }
    }
    class handleDeviceChangeException implements IPubSubEventCallback {

        @Override
        public void handleEvent(String topic, String type, Object obj) {

        }
    }

}
