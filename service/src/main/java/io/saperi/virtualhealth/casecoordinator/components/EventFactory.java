package io.saperi.virtualhealth.casecoordinator.components;

import io.saperi.virtualhealth.caseagent.shared.event.CaseAgentConfirmAliveRequest;
import io.saperi.virtualhealth.caseagent.shared.event.CaseAgentLost;
import io.saperi.virtualhealth.casecoordinator.data.CaseData;
import io.saperi.virtualhealth.casecoordinator.shared.event.CoordinatorStarted;
import io.saperi.virtualhealth.casecoordinator.shared.event.CoordinatorStopped;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import io.saperi.virtualhealth.caseagent.shared.event.CensusRequest;

import java.util.Date;

@Slf4j
@Component
public class EventFactory {
    public CensusRequest createCensusRequestEvent(SharedContext ctx)
    {
        CensusRequest evt = new CensusRequest();
        evt.setRequesterAgentId(ctx.getAgentId());
        return evt;
    }

    public CoordinatorStarted createCoordinatorStartedEvent(SharedContext context)
    {
        CoordinatorStarted evt = new CoordinatorStarted();
        evt.setAgentId(context.getAgentId());
        evt.setInputTopic(context.getRootInputTopic());
        evt.setOutputTopic(context.getRootOutputTopic());
        evt.setStarted(context.getStarted());
        return evt;
    }

    public CoordinatorStopped createCoordinatorStoppedEvent(SharedContext context)
    {
        CoordinatorStopped evt = new CoordinatorStopped();
        evt.setAgentId(context.getAgentId());
        evt.setStopped(new Date());
        return evt;
    }

    public CaseAgentConfirmAliveRequest createCaseAgentConfirmAliveRequest(SharedContext ctx, CaseData caseData)
    {
        CaseAgentConfirmAliveRequest evt = new CaseAgentConfirmAliveRequest();
        evt.setRequesterAgentId(ctx.getAgentId());
        evt.setAgentId(caseData.getAgentId());
        return evt;
    }

    public CaseAgentLost createCaseAgentLost(SharedContext ctx, CaseData caseData)
    {
        CaseAgentLost evt = new CaseAgentLost();
        //evt.
        return evt;
    }
}
