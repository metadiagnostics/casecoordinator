package io.saperi.virtualhealth.casecoordinator.components;

import com.fasterxml.uuid.Generators;
import io.saperi.virtualhealth.casecoordinator.data.AliveRecord;
import io.saperi.virtualhealth.casecoordinator.data.CaseData;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class SharedContext {

    @Value("${saperi.virtualhealth.domain}")
    private String domain;
    private final Date started;
    private String agentId;
    private String inputId;
    private String outputId;
    private String rootInputTopic;
    private String rootOutputTopic;
    private String managementTopic;
    private String keepAliveTopic;

    private Map<String, CaseData> caseMap =  new ConcurrentHashMap<String, CaseData>();
    private Map<String, Date> removedAgentsMap =  new ConcurrentHashMap<>();
    private Map<String, AliveRecord> lastSeensMap =  new ConcurrentHashMap<>();

    SharedContext()
    {
        started = new Date();
    }

    @PostConstruct
    public void configure()
    {
        UUID uuid = Generators.timeBasedGenerator().generate();
        agentId = uuid.toString();
        inputId = Generators.timeBasedGenerator().generate().toString();
        outputId = Generators.timeBasedGenerator().generate().toString();
        rootInputTopic = domain+"/"+inputId;
        rootOutputTopic = domain+"/"+outputId;
        managementTopic = domain + "/CCMgmt";
        keepAliveTopic = domain +"/CCAlive";
    }

    public Map<String, CaseData> getCaseMap() {
        return caseMap;
    }

    public String getKeepAliveTopic() {
        return keepAliveTopic;
    }

    public Map<String, AliveRecord> getLastSeensMap() {
        return lastSeensMap;
    }

    public String getManagementTopic() {
        return managementTopic;
    }

    public Map<String, Date> getRemovedAgentsMap() {
        return removedAgentsMap;
    }

    public String getRootInputTopic()
    {
        return rootInputTopic;
    }

    public String getAgentId() {
        return agentId;
    }

    public String getDomain() {
        return domain;
    }

    public String getRootOutputTopic() {
        return rootOutputTopic;
    }

    public Date getStarted() {
        return started;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }
}
