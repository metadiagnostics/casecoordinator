package io.saperi.virtualhealth.casecoordinator.handlers;

import io.saperi.common.communications.listeners.TypeMappedListener;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * This class handles all events on the Private Input Channel
 */
@Component
public class PrivateInputHandler extends TypeMappedListener
{
    @PostConstruct
    public void setupHandlers()
    {

    }


}
