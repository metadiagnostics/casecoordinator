package io.saperi.virtualhealth.casecoordinator.data;

import io.saperi.virtualhealth.caseagent.shared.data.PatientDemographics;
import lombok.Data;

@Data
public class CaseData {
    private String agentId;
    private String caseRequestTopic;
    private String caseGeneralInfoTopic;
    //TODO: Add Devices etc
    private PatientDemographics patientInfo;
}
