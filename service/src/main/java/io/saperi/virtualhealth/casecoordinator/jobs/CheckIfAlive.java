package io.saperi.virtualhealth.casecoordinator.jobs;

import io.saperi.common.communications.interfaces.IPubSubProvider;
import io.saperi.virtualhealth.caseagent.shared.event.CaseAgentConfirmAliveRequest;
import io.saperi.virtualhealth.casecoordinator.components.EventFactory;
import io.saperi.virtualhealth.casecoordinator.components.SharedContext;
import io.saperi.virtualhealth.casecoordinator.data.AliveRecord;
import io.saperi.virtualhealth.casecoordinator.data.CaseData;
import lombok.extern.slf4j.Slf4j;
import org.quartz.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Map;

@Slf4j
@Component
public class CheckIfAlive implements Job {
    private final IPubSubProvider pubSubSrv;
    private final SharedContext context;
    private final EventFactory eventFactory;
    @Value("${saperi.virtualhealth.caseagent.verifyalive:90000}")
    private long verifyAlive;
    @Value("${saperi.virtualhealth.caseagent.considerdead:300000}")
    private long considerDead;


    public CheckIfAlive(IPubSubProvider pubSubSrv, SharedContext context, EventFactory eventFactory) {
        this.pubSubSrv = pubSubSrv;
        this.context = context;
        this.eventFactory = eventFactory;
    }

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        log.info("Checking of alive");
        if (pubSubSrv != null)
        {
            log.info("We have a pub/sub interface proving that injection works");
        }
        else
        {
            log.info("Injection does not seem to be working on a job");
        }

        Date now = new Date();
        Map<String, AliveRecord> lastSeenMap = context.getLastSeensMap();
        Map<String, CaseData> caseMap = context.getCaseMap();
        for (String key: caseMap.keySet())
        {
            if (lastSeenMap.containsKey(key)) {
                AliveRecord data = lastSeenMap.get(key);
                long delta = now.getTime() - data.getReceived().getTime();
                if (delta>considerDead)
                {
                    handleDeadCaseAgent(caseMap.get(key));
                }
                else if ((verifyAlive>0) && (delta>verifyAlive))
                {
                    handleVerifyCaseAgent(caseMap.get(key));
                }
            }
            else
            {
                //We have not seen a keep alive signal from this case
                //First we create a marker for this case and put it in the lastSeenMap
                //This mark will claim the element was last seen at the now - marker time
                //We will then explicitly request an update
            }
        }
    }

    private void handleDeadCaseAgent(CaseData caseData)
    {
        // Tell folks it is dead
        // Cleanup resource associated with the case
    }

    private void handleVerifyCaseAgent(CaseData caseData )
    {
        CaseAgentConfirmAliveRequest evt = eventFactory.createCaseAgentConfirmAliveRequest(context, caseData);
        pubSubSrv.publishToTopic(caseData.getCaseRequestTopic(),evt);
    }



}
