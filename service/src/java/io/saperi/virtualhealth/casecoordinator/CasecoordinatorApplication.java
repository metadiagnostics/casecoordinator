package io.saperi.virtualhealth.casecoordinator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CasecoordinatorApplication {

    public static void main(String[] args) {
        SpringApplication.run(CasecoordinatorApplication.class, args);
    }

}
