package io.saperi.virtualhealth.casecoordinator.shared.event;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.saperi.common.communications.event.EventCommon;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include. NON_NULL)
abstract public class CoordinatorCommon extends EventCommon {

    private String agentId;

      public CoordinatorCommon()
    {

    }

    public CoordinatorCommon(String agentId)
    {
        this.agentId = agentId;
    }


}
