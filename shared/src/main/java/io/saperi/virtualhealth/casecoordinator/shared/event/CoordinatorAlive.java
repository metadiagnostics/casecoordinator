package io.saperi.virtualhealth.casecoordinator.shared.event;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.Date;

@Data
@JsonInclude(JsonInclude.Include. NON_NULL)
public class CoordinatorAlive {
    private Date time = new Date();
}
