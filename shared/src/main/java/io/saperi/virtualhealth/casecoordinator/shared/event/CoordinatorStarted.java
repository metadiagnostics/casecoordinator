package io.saperi.virtualhealth.casecoordinator.shared.event;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.Date;

@Data
@JsonInclude(JsonInclude.Include. NON_NULL)
public class CoordinatorStarted extends CoordinatorCommon {
    private Date started;
    private String inputTopic;
    private String outputTopic;
}
